import org.junit.jupiter.api.*;
import unittest.model.Movie;
import unittest.model.Race;
import unittest.model.Ring;
import unittest.model.TolkienCharacter;
import unittest.services.DataService;

import java.time.Duration;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static unittest.model.Race.*;

class DataServiceTest {

    DataService dataService;

    @BeforeEach()
    void setUp() {
        dataService = new DataService();
    }

    @Test
    public void ensureThatInitializationOfTolkienCharactersWorks() {
        TolkienCharacter frodo = new TolkienCharacter("Frodo", 33, HOBBIT);

        // TODO check that age is 33
        assertEquals(33, frodo.age, "Frodo should be 33");
        // TODO check that name is "Frodo"
        assertEquals("Frodo", frodo.getName(), "Name should be Frodo");
        // TODO check that name is not "Frodon"
        assertNotEquals("Frodon", frodo.getName(), "Name should be different than Frodon");
    }

    @Test
    void ensureThatEqualsWorksForCharacters() {
        Object jake = new TolkienCharacter("Jake", 43, HOBBIT);
        Object sameJake = jake;
        Object jakeClone = new TolkienCharacter("Jake", 12, HOBBIT);
        // TODO check that:
        // jake is equal to sameJake
        assertEquals(jake, sameJake, "Both variables should point at the same object");
        // jake is not equal to jakeClone
        assertNotEquals(jakeClone, jake, "JakeClone should not point to the jake object");
    }

    @Test
    void checkInheritance() {
        TolkienCharacter tolkienCharacter = dataService.getFellowship().get(0);
        // TODO check that tolkienCharacter.getClass is not a movie class
        assertFalse(Movie.class.isAssignableFrom(tolkienCharacter.getClass()));
        assertTrue(TolkienCharacter.class.isAssignableFrom(tolkienCharacter.getClass()));
    }

    @Test
    void ensureFellowShipCharacterAccessByNameReturnsNullForUnknownCharacter() {
        // TODO implement a check that dataService.getFellowshipCharacter returns null for an
        // unknow felllow, e.g. "Lars"
        TolkienCharacter unknownFellow = dataService.getFellowshipCharacter("Lars");
        assertNull(unknownFellow);
    }

    @Test
    void ensureFellowShipCharacterAccessByNameWorksGivenCorrectNameIsGiven() {
        // TODO implement a check that dataService.getFellowshipCharacter returns a fellow for an
        // existing felllow, e.g. "Frodo"
        TolkienCharacter frodo = dataService.getFellowshipCharacter("Frodo");
        assertNotNull(frodo);
    }


    @Test
    void ensureThatFrodoAndGandalfArePartOfTheFellowship() {

        List<TolkienCharacter> fellowship = dataService.getFellowship();
        TolkienCharacter frodo = dataService.getFellowshipCharacter("Frodo");
        TolkienCharacter gandalf = dataService.getFellowshipCharacter("Gandalf");
        // TODO check that Frodo and Gandalf are part of the fellowship
        assertTrue(fellowship.contains(frodo));
        assertTrue(fellowship.contains(gandalf));

    }

    @Test
    void ensureThatOneRingBearerIsPartOfTheFellowship() {

        List<TolkienCharacter> fellowship = dataService.getFellowship();
        Map<Ring, TolkienCharacter> ringBearers = dataService.getRingBearers();
        boolean result = ringBearers.values().stream().anyMatch(man -> fellowship.contains(man));

        // TODO test that at least one ring bearer is part of the fellowship
        assertTrue(result);
    }

    // TODO Use @RepeatedTest(int) to execute this test 1000 times
    @Tag("slow")
    @RepeatedTest(1000)
    @DisplayName("Minimal stress testing: run this test 1000 times to ")
    void ensureThatWeCanRetrieveFellowshipMultipleTimes() {
        dataService = new DataService();
        assertNotNull(dataService.getFellowship());
    }

    @Test
    void ensureOrdering() {
        List<TolkienCharacter> fellowship = dataService.getFellowship();

        // ensure that the order of the fellowship is:
        String[] fellows = {"frodo", "sam", "merry", "pippin", "gandalf", "legolas", "gimli", "aragorn", "boromir"};

        for (int i = 0; i < fellows.length; i++) {
            assertTrue(fellowship.get(i).getName().equalsIgnoreCase(fellows[i]));
        }
    }

    @Test
    void ensureAge() {
        List<TolkienCharacter> fellowship = dataService.getFellowship();

        // TODO test ensure that all hobbits and men are younger than 100 years
        assertTrue(fellowship.stream().filter(fellow -> fellow.getRace().equals(HOBBIT) || fellow.getRace().equals(Race.MAN)).allMatch(mortal -> mortal.age < 100));

        // TODO also ensure that the elfs, dwars the maia are all older than 100 years
        assertTrue(fellowship.stream().filter(r -> r.getRace().equals(ELF) || r.getRace().equals(MAIA) || r.getRace().equals(DWARF)).allMatch(other -> other.age > 100));
    }

    @Test
    void ensureThatFellowsStayASmallGroup() {

        List<TolkienCharacter> fellowship = dataService.getFellowship();

        // TODO Write a test to get the 20 element from the fellowship throws an
        // IndexOutOfBoundsException
        assertThrows(IndexOutOfBoundsException.class, () -> fellowship.get(20));
    }

    @Test
    void ensureThatTestDoesNotRunLongerThan3Seconds() {

        assertTimeout(Duration.ofSeconds(3), () -> {
            dataService.update();
        });
    }
}