import java.util.Arrays;
import java.util.stream.Stream;

import converter.ConverterUtil;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.DynamicTest.dynamicTest;

class ConverterUtilTest {

    int[][] celsiusFahrenheitMapping = new int[][] { { 10, 50 }, { 40, 104 }, { 0, 32 } };

    @TestFactory
    Stream<DynamicTest> ensureThatCelsiusConvertsToFahrenheit() {
        return Arrays.stream(celsiusFahrenheitMapping).map(entry -> {
            // access celsius and fahrenheit from entry
            int celsius = entry[0];
            int fahrenheit = entry[1];
            return dynamicTest(celsius + " Celsius will be: " + fahrenheit, () -> {
                assertEquals(fahrenheit, ConverterUtil.convertCelsiusToFahrenheit(celsius));
            });
        });

    }
    @TestFactory
    Stream<DynamicTest> ensureThatFahrenheitToCelsiusConverts() {
        // TODO Write a similar test fahrenheit to celsius
        return Arrays.stream(celsiusFahrenheitMapping).map(entry -> {
            // access celsius and fahrenheit from entry
            int celsius = entry[0];
            int fahrenheit = entry[1];
            return dynamicTest(fahrenheit + " Farenheit converter to celcius: " + celsius, () -> {
                assertEquals(celsius, ConverterUtil.convertFahrenheitToCelsius(fahrenheit));
            });
        });
    }
}