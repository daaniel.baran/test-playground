import file.FileWriter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.Assert.*;

public class FileWriterTest {

    @Test
    void ensureThatPathFromTempDirISWritable(@TempDir Path path) {
        // Check if the path created by the TempDir extension is writable
        // Check `Files` API for this
        boolean isWritable = Files.isWritable(path);

        assertTrue(isWritable);
    }

    @Test
    void ensureThatAppendingContentToFileThatDoesNotExistThrowsException(@TempDir Path path) {
        String content = "testing content";
        assertThrows(IOException.class, () -> FileWriter.appendFile(path, content));

    }
    //Ensure that you can write to the file once you created it
    @Test
    void ensureThatWritingToCreatedFileIsPossible(@TempDir Path path) throws IOException {
        Path file = path.resolve("test.txt");
        FileWriter.createFile(file);
        FileWriter.appendFile(file, "test content");

        assertTrue(Files.isReadable(file));
    }

}
