import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import unittest.email.EmailValidator;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.fail;

class EmailValidatorTest {
    // TODO Write test for EmailValidator
    // The names of the methods should give you a pointer what to test for

    @Test
    public void ensureThatEmailValidatorReturnsTrueForValidEmail() {
        assertTrue(EmailValidator.isValidEmail("lars.vogel@gmail.com"));
    }

    @Test
    @DisplayName("Ensure that the usage of a subdomain is still valid, see https://en.wikipedia.org/wiki/Subdomain")
    public void emailValidator_CorrectEmailSubDomain_ReturnsTrue() {
        assertTrue(EmailValidator.isValidEmail("lars.vogel@analytics.gmail.com"));
    }

    @Test
    @DisplayName("Ensure that a missing top level domain returns false")
    public void emailValidator_InvalidEmailNoTld_ReturnsFalse() {
        assertFalse(EmailValidator.isValidEmail("lars.vogel@gmail"));
    }

    @Test
    public void ensureThatDoubleDotInEmailReturnsFalse() {
        assertFalse(EmailValidator.isValidEmail("lars..vogel@gmail..com"));
        assertTrue(EmailValidator.isValidEmail("lars..vogel@gmail.com"));
    }

    @Test
    public void ensureThatEmailWithoutUserNameReturnsFalse() {
        assertFalse(EmailValidator.isValidEmail("@gmail.com"));
    }

    @Test
    public void ensureThatEmptyStringReturnsFalse() {
        assertFalse(EmailValidator.isValidEmail(""));
    }

    @Test
    public void ensureThatNullEmailReturnsFalse() {
        assertFalse(EmailValidator.isValidEmail(null));
    }

}